# Android UVC协议摄像头预览、截图与录屏Demo

## 概述

此开源项目旨在为Android开发者提供一个实用的示范，展示如何在Android设备上实现对基于USB视频类（UVC, USB Video Class）协议的外部摄像头进行实时预览，并支持截图和录屏功能。UVC协议是USB设备中广泛采用的标准，允许设备无需额外驱动就能被主机系统识别并使用。本Demo特别适用于那些需要集成外部摄像头应用的场景，如安防监控、专业摄影或工业检测等领域。

## 功能特点

- **UVC摄像头预览**：演示如何通过USB接口连接并预览UVC协议的摄像头画面。
- **截图功能**：允许用户捕捉当前预览的画面并保存至设备存储。
- **录屏能力**：集成录屏功能，可记录摄像头的预览视频到设备，便于后续查看或分析。
- **兼容性测试**：针对不同品牌与型号的Android设备进行了广泛的兼容性考虑。
- **示例代码清晰**：代码结构简洁明了，注释详尽，方便快速理解和二次开发。

## 快速入门

### 环境需求

- Android Studio开发环境
- Android SDK版本 >= 23
- 测试设备需支持USB OTG（On-The-Go），用于连接UVC摄像头

### 获取和运行

1. **克隆项目**：使用Git将本仓库克隆到本地。
   
   ```
   git clone https://github.com/your-repo-url.git
   ```

2. **导入到Android Studio**：打开Android Studio，选择"Open an existing Android Studio project"，然后导航到你刚才克隆的目录打开项目。
   
3. **配置权限**：确保AndroidManifest.xml文件中有如下权限：

   ```xml
   <uses-permission android:name="android.permission.CAMERA"/>
   <uses-permission android:name="android.permission.RECORD_AUDIO"/>
   <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
   <!-- 如果目标API级别高于29（Android 10），需要适配Scoped Storage -->
   ```
   
4. **连接UVC摄像头**：确保你的设备已通过USB OTG线连接好UVC摄像头。
5. **运行应用**：点击运行按钮，在支持USB摄像头的设备上预览并测试各项功能。

## 注意事项

- 对于Android 10及以上版本，可能需要处理Scoped Storage的相关限制来正确保存截图和录制的视频。
- 实际部署时，请根据目标硬件和使用场景调整代码以达到最佳性能。
- UVC设备种类繁多，尽管本Demo力求广泛兼容，但在特定设备上可能会遇到兼容性问题，需要针对性调试。

## 开源贡献

欢迎贡献代码改进、报告问题或提出建议。让我们共同完善这个项目，使其更加健壮和有用。

---

开始探索吧！让Android设备的扩展能力得到充分利用，无论是技术探索还是实际应用开发，都希望这个Demo能为你提供有效的帮助。